package se.comhem.test.montyhall.simulation;

public class MontyHallSimulationResult {
    private boolean victory;

    public MontyHallSimulationResult(boolean victory) {
        this.victory = victory;
    }

    public void setVictory(boolean victory) {
        this.victory = victory;
    }
    public boolean getVictory() {
        return victory;
    }
}
