package se.comhem.test.montyhall.simulation;

import java.util.ArrayList;
import java.util.Random;

public class MontyHallSimulation {
    private boolean isSwitchingDoor;
    private int numberOfSimulations;

    public MontyHallSimulation(boolean isSwitchingDoor, int numberOfSimulations) {
        this.isSwitchingDoor = isSwitchingDoor;
        this.numberOfSimulations = numberOfSimulations;
    }


    private boolean doSimulation() {
        ArrayList<Integer> doors = new ArrayList<Integer>();
        for (int i = 0; i < 3; i++) {
            doors.add(i);
        }
        Random random = new Random();

        int doorWithPrize = random.nextInt(doors.size());
        int firstPick = random.nextInt(doors.size());

        boolean isWinning = isSwitchingDoor ? firstPick != doorWithPrize : firstPick == doorWithPrize;
        return isWinning;
    }

    public ArrayList<MontyHallSimulationResult> runSimulations(){
        ArrayList<MontyHallSimulationResult> simulations = new ArrayList<MontyHallSimulationResult>();
        for (int i = 0; i < numberOfSimulations; i++) {
            boolean win = doSimulation();
            MontyHallSimulationResult simulation = new MontyHallSimulationResult(win);
            simulations.add(simulation);
        }
        return simulations;
    }
}
