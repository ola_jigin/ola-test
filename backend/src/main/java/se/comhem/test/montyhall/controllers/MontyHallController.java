package se.comhem.test.montyhall.controllers;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import se.comhem.test.montyhall.simulation.MontyHallSimulation;
import se.comhem.test.montyhall.simulation.MontyHallSimulationResult;

import java.util.ArrayList;

@RestController
public class MontyHallController {
    @RequestMapping(value = "/monty-hall", method = RequestMethod.GET)
    public ArrayList<MontyHallSimulationResult> montyHallSimulation(@RequestParam int numberOfSimulations, @RequestParam boolean isSwitchingDoor) {
        MontyHallSimulation montyHallSimulation = new MontyHallSimulation(isSwitchingDoor, numberOfSimulations);
        ArrayList<MontyHallSimulationResult> result = montyHallSimulation.runSimulations();
        return result;
    }
}
