package se.comhem.test.montyhall;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class MontyHallControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void testValidMontyHallSimulation() throws Exception {
        String numberOfSimulations = "1";
        String isSwitchingDoor = "false";

        mockMvc.perform(get("/monty-hall")
                .param("numberOfSimulations", numberOfSimulations)
                        .param("isSwitchingDoor", isSwitchingDoor)
                )
                .andExpect(status().isOk())
                .andExpect(content().contentType("application/json"))
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].victory").isBoolean());
    }

    @Test
    public void invalidQueryParameters() throws Exception {
        String isSwitchingDoor = "false";
        mockMvc.perform(get("/monty-hall")
                .param("isSwitchingDoor", isSwitchingDoor))
                .andExpect(status().is(400));
    }
}
