import React from "react";
import PropTypes from "prop-types";
import "./Button.css";

function Button({ text, onClick, type, disabled }) {
  return (
    <>
      <button
        type={type}
        onClick={onClick}
        disabled={disabled}
        className="button"
      >
        {text}
      </button>
    </>
  );
}

Button.propTypes = {
  text: PropTypes.string,
  onClick: PropTypes.func,
  type: PropTypes.string,
  disabled: PropTypes.bool,
};

export default Button;
