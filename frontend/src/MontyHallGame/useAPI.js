import { useState } from "react";
import axios from "axios";

export default function useAPI() {
  const [data, setData] = useState(null);
  const [isLoading, setIsLoading] = useState(false);
  const [isError, setIsError] = useState(false);

  const fetchData = async (url) => {
    setIsLoading(true);
    try {
      const response = await axios.get(url);
      setData(response.data);
    } catch (e) {
      console.log("Error when fetching data");
      console.log(e);
      setIsError(true);
    }
    setIsLoading(false);
  };

  return [{ data, isLoading, isError }, fetchData];
}
