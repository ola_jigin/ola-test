import React from "react";
import axios from "axios";
import { act, render, screen } from "@testing-library/react";
import userEvent from "@testing-library/user-event";

jest.mock("axios");
import MontyHallGame from "./MontyHallGame";

describe("MontyHallGame", () => {
  test("renders MontyHallGame", () => {
    render(<MontyHallGame />);

    expect(
      screen.getByText("Hur många simuleringar vill du göra?")
    ).toBeInTheDocument();
    expect(screen.getByText("Byt dörr")).toBeInTheDocument();
    expect(screen.getByText("Kör simulering")).toBeInTheDocument();
  });

  test("Runs a simulation", async () => {
    const simulations = [
      { victory: true },
      { victory: false },
      { victory: true },
    ];
    const promise = Promise.resolve({ data: simulations });
    axios.get.mockImplementationOnce(() => promise);

    render(<MontyHallGame />);
    const input = screen.getByPlaceholderText("Ange antal simuleringar");
    // Set to run 3 simulation
    userEvent.type(input, "3");
    expect(input.value).toBe("3");

    // Switch door
    const checkbox = screen.getByTestId("doorCheckbox");
    userEvent.click(checkbox);

    const runSimulationButton = screen.getByText("Kör simulering");
    userEvent.click(runSimulationButton);
    await act(() => promise);

    // Ensure we have a result
    expect(screen.getByText("Resultat")).toBeInTheDocument();

    // Show detailed result
    const showDetailsButton = screen.getByText("Visa alla simuleringar");
    userEvent.click(showDetailsButton);

    expect(screen.getAllByTestId("simulationResult")).toHaveLength(3);
  });
});
