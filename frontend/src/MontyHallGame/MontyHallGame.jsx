import React, { useState } from "react";
import useAPI from "./useAPI";
import Form from "../Form/Form";
import ErrorNotification from "../ErrorNotification/ErrorNotification";
import Result from "../Result/Result";

function MontyHallGame() {
  const [numberOfSimulations, setNumberOfSimulations] = useState("");
  const [isSwitchingDoor, setIsSwitchingDoor] = useState(false);
  const [{ data, isLoading, isError }, fetchData] = useAPI();

  const onInputChange = (e) => {
    e.target.name === "isSwitchingDoor"
      ? setIsSwitchingDoor(!isSwitchingDoor)
      : setNumberOfSimulations(e.target.value);
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    const url = `monty-hall?numberOfSimulations=${numberOfSimulations}&isSwitchingDoor=${isSwitchingDoor}`;
    fetchData(url);
  };

  return (
    <>
      <Form
        numberOfSimulations={numberOfSimulations}
        isSwitchingDoor={isSwitchingDoor}
        isLoading={isLoading}
        handleSubmit={handleSubmit}
        onInputChange={onInputChange}
      />
      {isError && (
        <ErrorNotification
          errorText={"Det blev fel, försök köra en ny simulering"}
        />
      )}
      {data && Array.isArray(data) && <Result result={data} />}
    </>
  );
}

export default MontyHallGame;
