import React, { useState } from "react";
import PropTypes from "prop-types";
import Button from "../Button/Button";

function Result({ result }) {
  const [showDetails, setShowDetails] = useState(false);

  const numberOfWins = result.map(({ victory }) => victory).filter(Boolean)
    .length;
  const numberOfSimulations = result.length;
  const winPercentage = parseFloat(
    (numberOfWins / numberOfSimulations) * 100
  ).toFixed(2);
  const btnText = showDetails
    ? "Dölj alla simuleringar"
    : "Visa alla simuleringar";

  return (
    <>
      <h2>Resultat</h2>
      <p>
        Du körde <strong>{numberOfSimulations}</strong> simuleringar vann{" "}
        <strong>{numberOfWins}</strong> gånger vilket ger en vinst chans på{" "}
        <strong>{winPercentage}%</strong>.
      </p>
      <p>Testa att prova igen för att se om det går bättre!</p>
      <Button
        text={btnText}
        onClick={(e) => {
          e.preventDefault();
          setShowDetails(!showDetails);
        }}
      />
      {showDetails && (
        <div>
          {result.map(({ victory }, idx) => (
            <SimulationResult key={idx} victory={victory} run={idx + 1} />
          ))}
        </div>
      )}
    </>
  );
}

Result.propTypes = {
  result: PropTypes.arrayOf(PropTypes.object),
};

function SimulationResult({ victory, run }) {
  return (
    <p data-testid="simulationResult">
      <strong>{run}</strong> {victory ? "vinst" : "förlust"}
    </p>
  );
}

SimulationResult.propTypes = {
  victory: PropTypes.bool,
  run: PropTypes.number,
};

export default Result;
