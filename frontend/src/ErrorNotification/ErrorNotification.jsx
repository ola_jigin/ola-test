import React from "react";
import PropTypes from "prop-types";

function ErrorNotification({ errorText }) {
  return (
    <div>
      <p>{errorText}</p>
    </div>
  );
}

ErrorNotification.propTypes = {
  errorText: PropTypes.string,
};

export default ErrorNotification;
