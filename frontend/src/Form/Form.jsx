import React from "react";
import PropTypes from "prop-types";

import Button from "../Button/Button";

import "./Form.css";

function Form({
  numberOfSimulations,
  isSwitchingDoor,
  isLoading,
  handleSubmit,
  onInputChange,
}) {
  return (
    <>
      <form onSubmit={handleSubmit}>
        <div className="field">
          <label>
            Hur många simuleringar vill du göra?
            <input
              placeholder="Ange antal simuleringar"
              type="number"
              onChange={onInputChange}
              min="1"
              max="10000"
              value={numberOfSimulations}
              name="numberOfSimulations"
            />
          </label>
        </div>

        <div className="field">
          <label>
            <input
              type="checkbox"
              checked={isSwitchingDoor}
              onChange={onInputChange}
              name="isSwitchingDoor"
              data-testid="doorCheckbox"
            />
            <span className="checkbox-label">Byt dörr</span>
          </label>
        </div>
        <Button text="Kör simulering" type="submit" disabled={isLoading} />
      </form>
    </>
  );
}

Form.propTypes = {
  numberOfSimulations: PropTypes.string,
  isSwitchingDoor: PropTypes.bool,
  isLoading: PropTypes.bool,
  handleSubmit: PropTypes.func,
  onInputChange: PropTypes.func,
};

export default Form;
